Pod::Spec.new do |s|
  s.name             = "VRSocialKit"
  s.version          = "1.2.1"
  s.summary          = "Useful set of timesaving tools"
  s.homepage         = "https://gitlab.com/voodoo-rocks/vrsocialkit-swift.git"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "Voodoo Rocks" => "public@voodoo-rocks.com" }
  s.source           = { :git => s.homepage, branch:'master', :tag => s.version.to_s }

  s.platform     = :ios, '9.0'
  s.requires_arc = true

  s.source_files = 'VRSocialKit/VR*.swift'

    s.frameworks = 'Foundation', 'AVFoundation', 'UIKit'
    s.dependency 'FBSDKCoreKit'
    s.dependency 'FBSDKLoginKit'
    s.dependency 'FBSDKShareKit'
    s.dependency 'InstagramKit'

end

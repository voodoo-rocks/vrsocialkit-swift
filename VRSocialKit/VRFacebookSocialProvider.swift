//
//  VRFacebookSocialProvider.swift
//  CoOpCube
//
//  Created by Alice on 14/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import FBSDKLoginKit
import UIKit

public class VRFacebookSocialProvider: NSObject, VRSocialProvider
{
    public var provider = "facebook"
    
    public func authorizeWithBlock(callback: VRSocialAuthBlock?) {
        if (FBSDKAccessToken.current() != nil) {
            self.getSocialUserWithCallback(callback)
        } else {
            FBSDKLoginManager().logIn(withReadPermissions: ["email"], from: UIApplication.shared.keyWindow?.rootViewController) { (_: FBSDKLoginManagerLoginResult?, error: Error?) in
                if error == nil {
                    self.getSocialUserWithCallback(callback)
                }
            }
        }
    }
    
    public func getSocialUserWithCallback(_ callback: VRSocialAuthBlock?) {
        FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields": "id, name, email, first_name, last_name"]).start { (connection: FBSDKGraphRequestConnection?, result: Any?, error: Error?) in
            if error != nil {
                print(error!.localizedDescription)
                return
            }
            
            let dictResult = result as! [String:String]
            let socialResponse = VRSocialResponse()
            socialResponse.userInfo = VRSocialUser()
            
            socialResponse.uid = dictResult["id"]
            socialResponse.token = FBSDKAccessToken.current().tokenString
            socialResponse.userInfo!.email = dictResult["email"]
            socialResponse.userInfo!.firstname = dictResult["first_name"]
            socialResponse.userInfo!.lastname = dictResult["last_name"]
            socialResponse.userInfo!.image = "https://graph.facebook.com/" + socialResponse.uid! + "/picture?type=large"
            socialResponse.userInfo!.username = dictResult["name"]
            
            if callback != nil {
                callback!(socialResponse)
            }
        }
    }
}

Pod::Spec.new do |s|
  s.name             = "VRSocialKit"
  s.version          = "1.0"
  s.summary          = "Useful set of timesaving tools"
  s.homepage         = "https://voodoo-mobile@bitbucket.org/voodoo-mobile/vrsocialkit-swift.git"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "Voodoo Mobile" => "public@voodoo-mobile.com" }
  s.source           = { :git => s.homepage, branch:'master', :tag => s.version.to_s }

  s.platform     = :ios, '9.0'
  s.requires_arc = true

  s.source_files = 'VRSocialKit/VR*.swift'

    s.frameworks = 'Foundation', 'AVFoundation', 'UIKit'
    s.dependency 'FBSDKLoginKit', '~> 4.14.0'
    s.dependency 'FBSDKShareKit', '~> 4.14.0'

end

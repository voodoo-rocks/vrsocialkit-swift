//
//  VRSocialResponse.swift
//  CoOpCube
//
//  Created by Alice on 12/07/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

public class VRSocialResponse {
    public var uid: String?
    public var token: String?
    public var provider: String?
    public var userInfo: VRSocialUser?
}

//
//  Bridging-Header.h
//  CoOpCube
//
//  Created by Alice on 14/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

#ifndef Bridging_Header_h
#define Bridging_Header_h

#import "FBSDKCoreKit/FBSDKCoreKit.h"
#import "FBSDKLoginKit/FBSDKLoginKit.h"
#import <InstagramKit/InstagramKit.h>

#endif /* Bridging_Header_h */

//
//  VRInstagramProvider.swift
//  Senate
//
//  Created by Alice on 21/12/2016.
//  Copyright © 2016 Voodoo Rocks. All rights reserved.
//

import InstagramKit

public class VRInstagramSocialProvider: NSObject, VRSocialProvider, UIWebViewDelegate {
    
    public var provider = "instagram"
    
    var socialCallback: VRSocialAuthBlock?
    
    fileprivate var webView = UIWebView()
    
    public func authorizeWithBlock(callback: VRSocialAuthBlock?) {
        if InstagramEngine.shared().accessToken != nil {
            if callback != nil {
                let socialResponse = VRSocialResponse()
                socialResponse.token = InstagramEngine.shared().accessToken
                callback!(socialResponse)
            }
            return;
        }
        
        socialCallback = callback
        let authURL = InstagramEngine.shared().authorizationURL()
        
        webView.delegate = self
        let keyWindow = UIApplication.shared.keyWindow!
        webView.frame = keyWindow.frame
        keyWindow.addSubview(webView)
        keyWindow.bringSubview(toFront: webView)
        webView.loadRequest(URLRequest(url: authURL))
        
        let closeButton = UIButton(frame: CGRect(x: 10, y: 10, width: 70, height: 30))
        closeButton.setTitle("Cancel", for: .normal)
        closeButton.addTarget(self, action: #selector(onCloseButton), for: .touchUpInside)
        webView.addSubview(closeButton)
    }
    
    @objc func onCloseButton() {
        webView.removeFromSuperview()
    }
    
    public func getSocialUserWithCallback(_ callback: VRSocialAuthBlock?) {
        
    }
    
    public func webView(_ webView: UIWebView, shouldStartLoadWith request:URLRequest, navigationType: UIWebViewNavigationType) -> Bool
    {
        do {
            try InstagramEngine.shared().receivedValidAccessToken(from: request.url!)
            
            webView.removeFromSuperview()
            if socialCallback != nil {
                let socialResponse = VRSocialResponse()
                socialResponse.token = InstagramEngine.shared().accessToken
                socialCallback!(socialResponse)
            }
        } catch {
            // first call always fails, second works TODO distinguish between them somehow
            //print("Error in Instagram token receiving: " + error.localizedDescription)
        }
        return true
    }
}

//
//  VRSocialUser.swift
//  CoOpCube
//
//  Created by Alice on 14/06/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

public class VRSocialUser {
    public var email: String?
    public var firstname: String?
    public var lastname: String?
    public var name: String?
    public var username: String?
    public var image: String?
}

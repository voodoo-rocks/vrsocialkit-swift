//
//  VRSocialProvider.swift
//  CoOpCube
//
//  Created by Alice on 11/07/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

public typealias VRSocialAuthBlock = (_ socialResponse: VRSocialResponse?) -> Void

public protocol VRSocialProvider {
    var provider: String {get}
    func authorizeWithBlock(callback: VRSocialAuthBlock?)
}

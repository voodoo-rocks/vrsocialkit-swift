//
//  VRSharedManager.swift
//  CoOpCube
//
//  Created by Alice on 11/07/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

import UIKit

public class VRSocialManager {
    public static func authorizeWithProvider(provider: VRSocialProvider, callback: VRSocialAuthBlock?) {
        provider.authorizeWithBlock(callback: callback)
    }
}
